window.addEventListener('keydown',(e)=>{
    const audio = document.querySelector(`audio[data-key="${e.keyCode}"]`)
    const key = document.querySelector(`.button[data-key="${e.keyCode}"]`)
    if(!audio) return
    audio.currentTime = 0;
    audio.play()
    key.classList.add('key-pressed')
})

function removeTranstition(e){
    if(e.propertyName !== 'color') return
    this.classList.remove('key-pressed')
}

const button = document.querySelectorAll('.button')
button.forEach(key => key.addEventListener('transitionend', removeTranstition))
