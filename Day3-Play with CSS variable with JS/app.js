const inputs = document.querySelectorAll('input')
console.log(inputs)

function Update() {
    const suffixe = this.dataset.sizing ||''
    document.documentElement.style.setProperty(`--${this.name}`, this.value + suffixe)
}

inputs.forEach(input => {
    input.addEventListener('change', Update)
    input.addEventListener('mousemove', Update)
})