const secondHand = document.querySelector('.sec-hand')
const minuteHand = document.querySelector('.min-hand')
const hourHand = document.querySelector('.hour-hand')

function setDate(){
    const now = new Date();
    const second = now.getSeconds()
    const secondDeg = ((second/60) * 360) + 90
    secondHand.style.transform = `rotate(${secondDeg}deg)`
    
    const minute = now.getMinutes()
    const minuteDeg = ((minute/60) * 360) + 90
    minuteHand.style.transform = `rotate(${minuteDeg}deg)`

    const hour = now.getHours()
    const hourDeg = ((hour/24) * 360) + 90 
    hourHand.style.transform = `rotate(${hourDeg}deg)`
}

setInterval(setDate, 1000)

setDate()